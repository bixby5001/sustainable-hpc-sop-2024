# Sustainable HPC SOP 2024

## Transcontinentalidade Fotônica como Resiliẽncia Climática em HPC
Autores:

Zeh Sobrinho
sobrinhosj@zoho.com

- Resumo
A demanda global de eletricidade por centros de dados pode dobrar até 2026
Estimamos que os centros de dados, criptomoedas e inteligência artificial (IA)
consumiram cerca de 460 TWh de eletricidade em todo o mundo em 2022, quase
2% da demanda total global de eletricidade. Os centros de dados são uma parte
fundamental da infraestrutura que suporta a digitalização, juntamente com a
infraestrutura elétrica que os alimenta. A quantidade cada vez maior de dados
digitais requer uma expansão e evolução dos centros de dados para processá-los e
armazená-los.
A demanda de eletricidade nos centros de dados é principalmente de dois
processos, sendo que a computação representa 40% da demanda de eletricidade
de um centro de dados. Os requisitos de refrigeração para alcançar uma eficiência
de processamento estável representam aproximadamente outros 40%. Os 20%
restantes vêm de outros equipamentos de TI associados.
As tendências futuras do setor de centros de dados são complexas de navegar, à
medida que os avanços tecnológicos e os serviços digitais evoluem rapidamente.
Dependendo do ritmo de implantação, da gama de melhorias de eficiência, bem
como das tendências da inteligência artificial e das criptomoedas, esperamos que o
consumo global de eletricidade dos centros de dados, criptomoedas e inteligência
artificial varie entre 620-1.050 TWh em 2026, com nosso cenário base para a
demanda em pouco mais de 800 TWh - em comparação com 460 TWh em 2022.
Isso corresponde a uma demanda adicional de eletricidade de 160 TWh a 590 TWh
em 2026 em comparação com 2022.
Embora a eletrificação de carros e veículos leves seja menos rápida no cenário
"New Momentum", ainda há cerca de 500 milhões desses veículos até 2035 e 1,4
bilhão até 2050, com os carros de passageiros elétricos representando cerca de
40% das vendas de novos carros em 2035 e 70% em 2050.
A eletrificação do transporte rodoviário está levando a um parque global de veículos
de 1-2 bilhões de veículos elétricos até 2050, o que implica um aumento na
demanda anual de capacidade de bateria dentro do transporte rodoviário de 10-20
Twh, portanto, para atender à demanda anual até 2050, seria necessária uma
potência solar contínua entre 1,14 TW e 2,28 TW. Isso significa que, em média,

precisamos gerar entre 1,14 e 2,28 terawatts de energia solar num cenário de
demanda elétrica total 60000 twh, sendo 40000 twh solar
O sol entrega 86000 TW em pequenos pacotes de 2 a 5 horas por país/região
enquanto todo o planeta consome 20W em um mix atual onde a queima de
hidrocarbonetos em térmicas representa 50%. Com uma eficiência energética de
50%, o power losses, 50%, produz aquecimento global imediato e perdas anuais de
US$17 trilhões.
Enquanto isto eficiência a energética em centros de computação de alto
desempenho (HPC) com backup a diesel, virou um mantra e um trauma desde o
“Gartner Says 50 Percent of Data Centers Will Have Insufficient Power and Cooling
Capacity by 2008”, adiado depois da quebra do Lehman Brothers um ano antes, para 19
de julho de 2022 em Londres.
É crucial para a sustentabilidade, tanto em termos econômicos quanto ambientais.
Este trabalho propõe uma abordagem inovadora para suportar a demanda de
energia utilizando a dualidade onda-partícula da luz. A proposta consiste em
transportar energia excedente de regiões com alta incidência solar para regiões com
menor insolação, utilizando o transporte de fótons. A energia solar capturada é
dividida em duas partes: 50% é convertido em energia elétrica para uso local e 50%
é convertido em fótons excedentes exportáveis para outros locais via um algoritmo
quântico baseado em qubits. Isso visa minimizar as perdas de transmissão de
energia de 18% em longas distâncias, promovendo uma infraestrutura HPC mais
sustentável e eficiente e redundantes ‘prevalência cloud in memory’ dispensando
backup a diesel. Quando o sol voltar às outras pontas, devolve a energia consumida
à noite.
Palavras-chave: Eficiência Energética, Computação de Alto Desempenho, Energia
Solar, Transporte de Fótons, Dualidade Onda-Partícula, Algoritmos Quânticos,
Qubits.

1. Introdução
Dia 19 de julho de 2022, Londres atingiu 42,9oC pela primeira vez na história. O
Google Cloud, sem resiliência climática, leia-se redundância externa, saiu do ar.
Junto, governo, defesa, financial city, saúde, gamers e toda bolha dev. A culpa
recaiu sobre o sistema de ar condicionado.
Por outras causas, em 06 de dezembro de 2020, parte da internet bancária
brasileira parou porque a rede de agua gelada que abastecia datacenters no modelo
‘campus’ falhou, derrubando todo o campus. A culpa recai sobre o ar
condicionado.
Dia 03 de novembro de 2004 ventos derrubaram as duas maiores e mais antigas
árvores da minha rua, a um quarteirão de casa. Meses depois, mesmo com a ONS
operador energético nacional decretando volume de espera antienchentes de

88,7% do volume útil para o reservatório de passo alto, bacia jacuí, rio grande do
sul, todos reservatórios atingiram defluência e afluências recordes. Algumas, acima
de 12.000 m3/s ou 40x o normal. O datacenter da operadora Vivo e demais em
Porto Alegre foram alagados obrigando as empresas de telefonia, pela primeira vez
na história, abrirem o sinal. A culpa não foi do ar condicionado, que, juntamente
com a camada OSI, energia e refrigeração e aquecimento global, não caiu na prova,
restando, a culpa foi da falta de redundância externa.
Estas mudanças radicais afetam a matriz hídrica energética do Rio Grande do Sul e
brasileira, que, de geradora de energia, passa a se dedicar ao controle de cheias
através de drenagem interbacias, fazendo com que energia renovável hidráulica
assuma a missão, visão, valor e propósito de de controle do nível nos reservatórios
focado no agro, vidas- preventivo - pois drenagem nas pontas - corretivo - não é
mais suficiente.
A computação de alto desempenho (HPC) demanda grande quantidade de energia,
principalmente devido à necessidade de redundância e confiabilidade. Atualmente,
muitos centros de HPC dependem de geradores a diesel como fonte de energia
passiva ou primária, o que resulta em custos de geração elevados e impactos
ambientais significativos. O Brasil, com seu potencial solar de até 38 TW, oferece
uma oportunidade única para desenvolver soluções energéticas inovadoras e
sustentáveis para o HPC, contudo, a 5 horas do dia, tem impacto zero se não
interconectado globalmente.
Este trabalho explora o excedente de energia solar do Brasil e demais regiões
intercontinentais, notadamente desertos, para alimentar centros de HPC
globalmente, transportando fótons e reconvertendo-os em energia nas pontas. A
abordagem proposta visa minimizar as perdas associadas à transmissão de energia
elétrica, 18%, via cabos transatlânticos convencionais a primeira fase, até que
tenhamos redes intercontinentais óptico-energéticas, promovendo a eficiência
energética global.

2. Metodologia
2.1 Caso: Potencial Solar no Brasil
O Brasil possui um potencial solar de aproximadamente 5 horas diárias, gerando até
38 TW, comparado ao consumo mundial de 20 TW. Este excedente energético será
aproveitado para abastecer centros de HPC localizados em diferentes fusos
horários.
2.2 Transporte de Fótons
A técnica de transporte de fótons baseia-se na dualidade onda-partícula da luz. A
energia solar captada é convertida em fótons, que são transportados através de
fibra óptica ou outras tecnologias de transmissão de luz. Ao chegar ao destino, os
fótons são reconvertidos em energia elétrica ou iluminação para alimentar os
centros de HPC.
2.3 Algoritmo Quântico para Transporte de Fótons

O algoritmo quântico para transporte de fótons utiliza um sistema de qubits para
controlar a transmissão de fótons excedentes. Os qubits podem ser manipulados
para codificar informações sobre a direção e a intensidade da transmissão de
fótons, otimizando o processo de transporte. Essa abordagem permite um controle
preciso e eficiente do fluxo de energia, minimizando as perdas de energia durante a
transmissão.
2.4 Reconversão de Energia
A reconversão de fótons em energia utiliza células fotovoltaicas de alta eficiência e
dispositivos de conversão de luz em eletricidade. Essa abordagem minimiza as
perdas de transmissão que ocorrem em linhas de energia convencionais.

3. Resultados Esperados
A implementação da técnica proposta deve resultar em:
● Redução de custos: Diminuição dos custos de geração de energia em
comparação com geradores a diesel.
● Eficiência Energética: Aumento da eficiência na transmissão e uso da energia
solar excedente.
● Sustentabilidade Ambiental: Redução da pegada de carbono associada à
geração de energia para centros de HPC.

4. Discussão
A viabilidade técnica e econômica do transporte de fótons precisa ser avaliada em
termos de custo de infraestrutura e eficiência de conversão. Além disso, questões
de segurança e manutenção da infraestrutura de transmissão de luz devem ser
abordadas. O desenvolvimento de algoritmos quânticos robustos e eficientes para o
controle do transporte de fótons é crucial para o sucesso da abordagem proposta.

5. Conclusão
A proposta de transporte de fótons para alimentar centros de HPC em regiões com
menor ou zero insolação, utilizando um sistema de qubits para o controle da
transmissão, apresenta uma solução inovadora e sustentável para o desafio da
eficiência energética. Com o potencial solar significativo do Brasil, essa abordagem
pode não apenas reduzir os custos operacionais dos centros de HPC, mas também
contribuir para a mitigação dos impactos ambientais associados ao consumo de
energia.

6. Trabalhos Futuros
Futuras pesquisas devem focar na otimização dos dispositivos de conversão de luz
em eletricidade, no desenvolvimento de algoritmos quânticos eficientes para o
controle do transporte de fótons, bem como na implementação de projetos-piloto
para validar a viabilidade prática dessa técnica.

Referências
[1] K. Kraemer, W. Hofmann, S. Schröder, et al., "Energy Efficiency in HPC Centers:
Towards Renewable Energy Integration," J. Supercomput., vol. 76, no. 7, pp.
5648-5664, 2020.
[1] Energy Efficiency Considerations for HPC Procurement Document: 2023
(revision 2.1)
[2] L. D. Silva, "Solar Energy Potential in Brazil: An Overview," Renewable Energy
Focus, vol. 33, pp. 1-6, 2019.
[3] S. R. Narang, "Photon Transport Technology: Innovations and Applications,"
Photonics J., vol. 12, no. 4, pp. 1542-1550, 2021.
[4] M. A. Nielsen and I. L. Chuang, Quantum Computation and Quantum Information,
10th Anniversary Edition, Cambridge University Press, 2010.

Biografia do Autor
Zeh Sobrinho é técnico laboratorial na startup Mellieri MPK, com interesses de
pesquisa focados em eficiência energética, energias renováveis e computação de
alto desempenho. Ele tem contribuído significativamente para o desenvolvimento de
soluções sustentáveis em infraestrutura de TI.
Nota: Este documento foi preparado para o Sustainable HPC State of the Practice
Workshop 2024, com o objetivo de compartilhar uma nova abordagem para a
sustentabilidade energética em centros de HPC.
Referências Externas:
● A proposta de investir em redundância externa em multi-regiões,
aproveitando o potencial solar interconectado globalmente, é uma abordagem
inovadora e promissora para aumentar a resiliência climática de sistemas
HPC.
● A ideia de interligar o planeta globalmente, aproveitando o excedente de
energia solar produzido em uma região para alimentar sistemas HPC em
regiões com menor insolação, é um conceito visionário que merece ser
explorado.
● O potencial solar global é vastamente superior ao consumo energético atual,
com 86000 TW de energia solar disponível e apenas 20 TW de consumo
global.
● O Brasil, com seu potencial solar de 38 TW, poderia desempenhar um papel
crucial nesse cenário, exportando energia solar para outras regiões.
● A transmissão de energia entre regiões, utilizando tecnologias de
transmissão de energia bidirecional pode ser uma abordagem de curto prazo
devido às perdas, sendo gradualmente substituído por linhas óticas
bidirecionais, como a conversão da energia em ondas eletromagnéticas,

pode minimizar as perdas tradicionais de energia durante o transporte. Essa
abordagem permitiria o fornecimento de energia confiável e sustentável para
sistemas HPC em diferentes regiões, independentemente da insolação local.
● A implementação dessa solução exigiria investimentos significativos em
infraestrutura de energia solar e sistemas de transmissão de energia dual. No
entanto, os benefícios de longo prazo, como a redução de custos
operacionais, a diminuição das emissões de gases de efeito estufa e a
garantia de um fornecimento de energia confiável e resiliente, justificam os
investimentos iniciais.
● A integração de sistemas HPC com redes de energia solar globalmente
interligadas representa um avanço significativo na busca por sistemas HPC
mais sustentáveis e resilientes. Essa abordagem inovadora pode revolucionar
a forma como os sistemas HPC são projetados e operados, contribuindo para
um futuro mais verde e sustentável.